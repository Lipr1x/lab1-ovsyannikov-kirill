# lab1 Ovsyannikov Kirill

## Материнская плата MSI B460M-A PRO
### Сильной стороной MSI B460M-A Pro является разъем М.2 с радиатором охлаждения. Это определенно позитивный момент в случае использования современных и скоростных накопителей. Тем более, что на MSI B460M-A Pro данный разъем поддерживает NVMe накопители

## Процессор Intel® Core™ i7-10700(F)
### Процессор Intel® Core™ i7 10-го поколения с разблокированным множителем обеспечивает плавный геймплей в самых ресурсоемких играх даже при одновременной записи и трансляции. 8 ядер, 16 потоков и максимальная тактовая частота 4,8 ГГц обеспечивают вычислительную мощность, необходимую для работы в многозадачном режиме.

## Видеокарта msi geforce rtx 3070 ventus 2x
### 10 Гбайт видеопамяти GDDR6X (вместо 11 Гбайт GDDR6 у RTX 2080 Ti) с более широким интерфейсом 320 бит, использует 8704 ядра CUDA (вдвое больше, чем у RTX 2080) в сочетании с GPU, работающем на частоте 1,71 ГГц в пике.

## Оперативная память 2 x 8GB HyperX FURY RGB DDR4-2666
### Хорошая недорогая и в то же время надежная оперативка

## SSD накопитель 500GB Kingston A2000

##  Жесткий диск 2TB Seagate BarraCuda

##  Блок питания Cooler Master V750 750W



#Данный компьютер один из самых лучших вариантов на конец 2020 года, нацелен в первую очередь на новинки игровой индустрии